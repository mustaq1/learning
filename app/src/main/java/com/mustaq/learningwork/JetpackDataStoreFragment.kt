package com.mustaq.learningwork

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment


class JetpackDataStoreFragment : Fragment() {
    companion object {

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val dataSourceView =
            inflater.inflate(R.layout.fragment_jetpack_data_store, container, false)


        return dataSourceView
    }


}