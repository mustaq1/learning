package com.mustaq.learningwork

import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.mustaq.learningwork.uils.disableView
import com.mustaq.learningwork.uils.enableViews
import java.util.concurrent.TimeUnit

class MobileOtpFragment : Fragment() {
    lateinit var edMobile: EditText
    lateinit var fieldVerificationCode: EditText
    lateinit var buttonStartVerification: Button
    lateinit var buttonVerifyPhone: Button
    lateinit var buttonResend: Button
    lateinit var signOutButton: Button
    lateinit var mobileNo: String

    private lateinit var auth: FirebaseAuth
    private var verificationInProgress = false
    private var storedVerificationId: String? = ""
    private lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    lateinit var mobileView: View
    lateinit var main_layout: LinearLayout
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mobileView = inflater.inflate(R.layout.fragment_mobile_otp, container, false)
        buttonStartVerification = mobileView.findViewById(R.id.buttonStartVerification)
        buttonVerifyPhone = mobileView.findViewById(R.id.buttonVerifyPhone)
        buttonResend = mobileView.findViewById(R.id.buttonResend)
        signOutButton = mobileView.findViewById(R.id.signOutButton)
        main_layout = mobileView.findViewById(R.id.main_layout)
        fieldVerificationCode = mobileView.findViewById(R.id.fieldVerificationCode)

        auth = Firebase.auth

        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                Log.e(TAG, "onVerificationCompleted: ")
                verificationInProgress = false
                updateUI(STATE_VERIFY_FAILED)
                signInWithPhoneAuthCredential(credential)
            }

            override fun onVerificationFailed(e: FirebaseException) {
                Log.w(TAG, "onVerificationFailed", e)
                verificationInProgress = false
                if (e is FirebaseAuthInvalidCredentialsException) {
                    Toast.makeText(requireActivity(), "Invalid phone number", Toast.LENGTH_SHORT)
                        .show()
                } else if (e is FirebaseTooManyRequestsException) {
                    Snackbar.make(
                        mobileView.findViewById(R.id.main_layout), "Quota exceeded.",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
                updateUI(STATE_VERIFY_FAILED)
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {
                Log.d(TAG, "onCodeSent:$verificationId")
                storedVerificationId = verificationId
                resendToken = token
                updateUI(STATE_CODE_SENT)

            }
        }
        return view
    }

    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        updateUI(currentUser)

        if (verificationInProgress && validatePhoneNumber()) {
            startPhoneNumberVerification(mobileNo)
        }
    }

    private fun startPhoneNumberVerification(mobileNo: String) {
        PhoneAuthProvider
            .getInstance()
            .verifyPhoneNumber(
                mobileNo,
                60,
                TimeUnit.SECONDS,
                requireActivity(),
                callbacks

            )
        verificationInProgress = true
    }


    private fun validatePhoneNumber(): Boolean {

        if (TextUtils.isEmpty(mobileNo)) {
            Snackbar.make(
                mobileView.findViewById(R.id.main_layout), "Invalid phone number..",
                Snackbar.LENGTH_SHORT
            ).show()
            return false
        }

        return true
    }

    private fun verifyPhoneNumberWithCode(verificationId: String?, code: String) {
        // [START verify_with_code]
        val credential = PhoneAuthProvider.getCredential(verificationId!!, code)
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential)
    }

    fun resendCode(phoneNumber: String, token: PhoneAuthProvider.ForceResendingToken?) {
        PhoneAuthProvider
            .getInstance()
            .verifyPhoneNumber(
                phoneNumber,
                60,
                TimeUnit.SECONDS,
                requireActivity(),
                callbacks,
                token
            )

    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        auth
            .signInWithCredential(credential)
            .addOnCompleteListener(requireActivity()) {
                if (it.isSuccessful) {
                    Log.d(TAG, "signInWithPhoneAuthCredential: Success")
                    val user = it.result.user
                    updateUI(STATE_SIGNIN_SUCCESS, user)
                } else {
                    Log.d(TAG, "signInWithPhoneAuthCredential: Failed ${it.exception}")
                    Snackbar.make(
                        mobileView
                            .findViewById(R.id.main_layout),
                        "Invaild Code",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
                updateUI(STATE_SIGNIN_FAILED)
            }
    }

    private fun signOut() {
        auth.signOut()
        updateUI(STATE_INITIALIZED)
    }


    fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            updateUI(STATE_SIGNIN_SUCCESS, user)
        } else {
            updateUI(STATE_INITIALIZED)
        }
    }

    private fun updateUI(uiState: Int, cred: PhoneAuthCredential) {
        updateUI(uiState, null, cred)
    }

    private fun updateUI(
        uiState: Int,
        user: FirebaseUser? = auth.currentUser,
        cred: PhoneAuthCredential? = null
    ) {
        when (uiState) {
            STATE_INITIALIZED -> {
                enableViews(
                    buttonStartVerification,
                    edMobile
                )
                disableView(buttonVerifyPhone, buttonResend)
            }
        }
    }


    companion object {
        val TAG = "MobileOtp"
        private const val KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress"
        private const val STATE_INITIALIZED = 1
        private const val STATE_VERIFY_FAILED = 3
        private const val STATE_VERIFY_SUCCESS = 4
        private const val STATE_CODE_SENT = 2
        private const val STATE_SIGNIN_FAILED = 5
        private const val STATE_SIGNIN_SUCCESS = 6
    }
}