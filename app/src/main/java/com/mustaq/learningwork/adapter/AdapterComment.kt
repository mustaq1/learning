package com.mustaq.learningwork.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.learningwork.model.Comment
import com.mustaq.learningwork.R
import kotlinx.android.synthetic.main.item_row.view.*

class AdapterComment : RecyclerView.Adapter<AdapterComment.CommentHolder>() {
    lateinit var postsList: List<Comment>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_row, parent, false)
        return CommentHolder(view)
    }

    override fun onBindViewHolder(holder: CommentHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return postsList.size
    }

    inner class CommentHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(position: Int) {
            with(postsList) {
                itemView.tvId.text = "id:-${this[position].id}"
            }
            position.apply {
                itemView.tvUserId.text = "Post Id:- ${postsList[this].postId}"
            }
            position.let {
                itemView.tvTitle.text = "Title:- ${postsList[it].email}"
            }
            position.also {
                itemView.tvBody.text = "Body:- ${postsList[it].body}"
            }
        }


    }

    fun setCommentList(postDataList: List<Comment>) {
        this.postsList = postDataList
        notifyDataSetChanged()
    }
}