package com.mustaq.learningwork.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.mustaq.learningwork.model.Post
import com.mustaq.learningwork.R
import com.mustaq.learningwork.adapter.AdapterPost.PostHolder
import kotlinx.android.synthetic.main.item_row.view.*

class AdapterPost : RecyclerView.Adapter<PostHolder>() {
    lateinit var postsList: List<Post>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_row, parent, false)
        return PostHolder(view)
    }

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return postsList.size
    }

    inner class PostHolder(itemView: View) : ViewHolder(itemView) {
        fun bind(position: Int) {


            with(postsList) {
                itemView.tvId.text = this[position].id.toString()
            }
            //itemView.tvId.text = "User id:-${postsList[position].id}"
            itemView.tvUserId.text = "Id:- ${postsList[position].userId}"
            itemView.tvTitle.text = "Title:- ${postsList[position].title}"
            itemView.tvBody.text = "Body:- ${postsList[position].bordy}"
        }


    }

    fun setPostList(postDataList: List<Post>) {
        this.postsList = postDataList
        notifyDataSetChanged()
    }
}