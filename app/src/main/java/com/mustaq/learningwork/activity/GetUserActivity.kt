package com.mustaq.learningwork.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.learningwork.model.Post
import com.mustaq.learningwork.R
import com.mustaq.learningwork.networkCall.RetrofitSingleTonObject
import com.mustaq.learningwork.adapter.AdapterPost
import kotlinx.android.synthetic.main.activity_get_user.*
import retrofit2.Call
import retrofit2.Response

class GetUserActivity : AppCompatActivity() {


    lateinit var btnGetUser: Button
    lateinit var recyclerView: RecyclerView
    lateinit var layoutManager: LinearLayoutManager
    lateinit var adapterPost: AdapterPost
    lateinit var editText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_user)


        recyclerView = findViewById(R.id.rvUserQuery)
        layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView.layoutManager = layoutManager
        recyclerView.setHasFixedSize(true)
        adapterPost = AdapterPost()
        btnGetUser = findViewById(R.id.buttonGetUser)

        btnGetUser.setOnClickListener {
            val postId = edUserID.text.toString()
            if (postId.isEmpty()) {
                Toast.makeText(this, "Please Enter User id", Toast.LENGTH_SHORT).show()
            } else {

                 getPostUsingId(Integer.parseInt(postId))
                //getPostUsingMultipleQuery(Integer.parseInt(postId))

                //getPostUsingMultipleQueryHasMap(Integer.parseInt(postId))
            }

        }

    }

    private fun getPostUsingMultipleQueryHasMap(parseInt: Int) {
        val map = HashMap<String, String>()
        map["userId"] = parseInt.toString()
        map["_sort"] = "id"
        map["_order"] = "desc"
        RetrofitSingleTonObject.getRetrofitInstance
            .getPostUsingQueryHasMap(map)
            .enqueue(object : retrofit2.Callback<List<Post>> {
                override fun onFailure(call: Call<List<Post>>, t: Throwable) {

                }

                override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            adapterPost.setPostList(response.body()!!)
                            recyclerView.adapter = adapterPost
                        }
                    }
                }

            })
    }

    private fun getPostUsingMultipleQuery(parseInt: Int) {

        RetrofitSingleTonObject.getRetrofitInstance
            .getPostUsingMultipleQuery(
                arrayOf(1, 3, 6),
                "id",
                "desc"
            )
            .enqueue(object : retrofit2.Callback<List<Post>> {
                override fun onFailure(call: Call<List<Post>>, t: Throwable) {

                }

                override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            adapterPost.setPostList(response.body()!!)
                            recyclerView.adapter = adapterPost
                        }
                    }
                }

            })
    }

    private fun getPostUsingId(userId: Int) {


        RetrofitSingleTonObject.getRetrofitInstance
            .getPostUsingQuery(userId)
            .enqueue(object : retrofit2.Callback<List<Post>> {
                override fun onFailure(call: Call<List<Post>>, t: Throwable) {

                }

                override fun onResponse(
                    call: Call<List<Post>>,
                    response: Response<List<Post>>
                ) {

                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            adapterPost.setPostList(response.body()!!)
                            recyclerView.adapter = adapterPost
                        }
                    }
                }

            })


    }
}
