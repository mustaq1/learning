package com.mustaq.learningwork.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.learningwork.model.Post
import com.mustaq.learningwork.R
import com.mustaq.learningwork.networkCall.RetrofitSingleTonObject
import com.mustaq.learningwork.adapter.AdapterPost
import retrofit2.Call
import retrofit2.Response

class SimpleApiCallActivity : AppCompatActivity() {
    lateinit var recyclerView: RecyclerView
    lateinit var adapterPost: AdapterPost


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_api_call)
        recyclerView = findViewById(R.id.rvSimplePost)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        adapterPost = AdapterPost()
        //getApiCall()
        createPost()
    }

    private fun createPost() {
        val post =
            Post(11, "Demo", "Demo For learning")
        RetrofitSingleTonObject.getRetrofitInstance
            .postData(post)
            .enqueue(object : retrofit2.Callback<Post> {
                override fun onFailure(call: Call<Post>, t: Throwable) {
                    Toast.makeText(
                        this@SimpleApiCallActivity,
                        "${t.message}", Toast.LENGTH_SHORT
                    ).show()
                }

                override fun onResponse(call: Call<Post>, response: Response<Post>) {
                    if (!response.isSuccessful) {
                        Toast.makeText(
                            this@SimpleApiCallActivity,
                            "Response Code ${response.code()}", Toast.LENGTH_SHORT
                        ).show()
                        return
                    }
                    val postResponse = response.body()
                    Log.e("TAG", postResponse!!.bordy.length.toString())
                    Toast.makeText(
                        this@SimpleApiCallActivity,
                        "Save Successfully..",
                        Toast.LENGTH_SHORT
                    ).show()
                }

            })
    }

    private fun getApiCall() {
        RetrofitSingleTonObject.getRetrofitInstance
            .post
            .enqueue(object : retrofit2.Callback<List<Post>> {
                override fun onFailure(call: Call<List<Post>>, t: Throwable) {

                    Log.e(MainActivity.TAG, "${t.message}")

                }

                override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            Log.e(MainActivity.TAG, "response ${response.body()}")
                            adapterPost.setPostList(response.body()!!)
                            recyclerView.adapter = adapterPost
                        } else {
                            Toast.makeText(
                                applicationContext,
                                "Something went wrong",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                }


            })
    }
}
