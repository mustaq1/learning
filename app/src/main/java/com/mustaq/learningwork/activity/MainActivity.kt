package com.mustaq.learningwork.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import com.google.android.material.navigation.NavigationView
import com.mustaq.learningwork.R

class MainActivity : AppCompatActivity(), View.OnClickListener {
    companion object {
        val TAG = "MainACtivity"
    }


    //1 first define nav controller in activity
    lateinit var navController: NavController


    //3 define drawer layout in activity
    lateinit var navigationView: NavigationView

    //2 define appbar config in activity
    lateinit var appBarConfiguration: AppBarConfiguration

    //9 create listener
    private lateinit var destinationListener: NavController.OnDestinationChangedListener


    lateinit var btnSimpleCall: Button
    lateinit var btnCommentData: Button
    lateinit var btnGetUser: Button
    lateinit var btnGotoVideo: Button
    lateinit var linearLayout: LinearLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        linearLayout = findViewById(R.id.llMain)
        linearLayout.apply {
            btnSimpleCall = this.findViewById(R.id.btnSimpleCall)
            btnCommentData = this.findViewById(R.id.btnCommentData)
            btnGetUser = this.findViewById(R.id.btnGetUser)
            btnGotoVideo = this.findViewById(R.id.btnGotoVideo)

            btnSimpleCall.setOnClickListener(this@MainActivity)
            btnCommentData.setOnClickListener(this@MainActivity)
            btnGetUser.setOnClickListener(this@MainActivity)
            btnGotoVideo.setOnClickListener(this@MainActivity)
        }


        //4 find nav controller in activity
        navController = findNavController(R.id.fragment)


    }


    override fun onClick(id: View) {

        when (id.id) {
            R.id.btnSimpleCall -> {
                val intent = Intent(this, SimpleApiCallActivity::class.java)
                startActivity(intent)

            }
            R.id.btnCommentData -> {
                val intent = Intent(this, CommentDataActivity::class.java)
                startActivity(intent)
            }
            R.id.btnGetUser -> {
                val intent = Intent(this, GetUserActivity::class.java)
                startActivity(intent)
            }
            R.id.btnGotoVideo -> {
                val intent = Intent(this, VideoUploadActivity::class.java)
                startActivity(intent)
            }

        }


    }

    override fun onNavigateUp(): Boolean {
        val navController = findNavController(R.id.fragment)
        return navController.navigateUp() || super.onNavigateUp()
    }


}