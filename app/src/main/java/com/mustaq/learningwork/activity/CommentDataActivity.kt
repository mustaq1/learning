package com.mustaq.learningwork.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mustaq.learningwork.R
import com.mustaq.learningwork.networkCall.RetrofitSingleTonObject
import com.mustaq.learningwork.adapter.AdapterComment
import com.mustaq.learningwork.model.Comment
import kotlinx.android.synthetic.main.activity_comment_data.*
import retrofit2.Call
import retrofit2.Response

class CommentDataActivity : AppCompatActivity() {

    lateinit var recyclerView: RecyclerView
    lateinit var adapterPost: AdapterComment
    lateinit var layoutManager: LinearLayoutManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comment_data)


        layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recyclerView = findViewById(R.id.rvComment)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager
        adapterPost = AdapterComment()
        buttonGet.setOnClickListener {
            if (edCommnetId.text!!.isEmpty()) {
                Toast.makeText(applicationContext, "Please enter comment id", Toast.LENGTH_SHORT)
                    .show()
            } else {
                //getComment(edCommnetId.text.toString())
                getCommentUsingUrl()
            }
        }
    }

    private fun getCommentUsingUrl() {
        RetrofitSingleTonObject.getRetrofitInstance
            .getCommentUsingUrl("posts/3/comments")
            .enqueue(object : retrofit2.Callback<List<Comment>> {
                override fun onFailure(call: Call<List<Comment>>, t: Throwable) {

                }

                override fun onResponse(
                    call: Call<List<Comment>>,
                    response: Response<List<Comment>>
                ) {

                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            adapterPost.setCommentList(response.body()!!)
                            recyclerView.adapter = adapterPost
                        } else {
                            Toast.makeText(
                                applicationContext,
                                "Something went wrong",
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }
                    }
                }

            })
    }

    private fun getComment(postId: String) {
        RetrofitSingleTonObject.getRetrofitInstance
            .getComment(postId.toInt())
            .enqueue(object : retrofit2.Callback<List<Comment>> {
                override fun onFailure(call: Call<List<Comment>>, t: Throwable) {

                }

                override fun onResponse(
                    call: Call<List<Comment>>,
                    response: Response<List<Comment>>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            adapterPost.setCommentList(response.body()!!)
                            recyclerView.adapter = adapterPost
                        } else {
                            Toast.makeText(
                                applicationContext,
                                "Something went wrong",
                                Toast.LENGTH_SHORT
                            )
                                .show()
                        }
                    }
                }

            })
    }
}
