package com.mustaq.learningwork.model;

public class Comment {
    int postId;
    int id;
    String name;
    String email;
    String body;

    public int getPostId() {
        return postId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getBody() {
        return body;
    }
}
