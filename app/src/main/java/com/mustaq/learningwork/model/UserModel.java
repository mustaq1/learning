package com.mustaq.learningwork.model;

public class UserModel {

    String firstName;
    String lastName;
    int age;
    int mobile;
    String gender;
    String city;
    String state;
    String country;
    String imageUrl;


    public UserModel() {
    }

    public UserModel(String _firstName, String _lastName, int _age, int _mobile, String _gender, String _city, String _state, String _country, String _imageUrl) {

        this.firstName = _firstName;
        this.lastName = _lastName;
        this.age = _age;
        this.mobile = _mobile;
        this.gender = _gender;
        this.city = _city;
        this.state = _state;
        this.country = _country;
        this.imageUrl = _imageUrl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getMobile() {
        return mobile;
    }

    public void setMobile(int mobile) {
        this.mobile = mobile;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
