package com.mustaq.learningwork.model;

public class Post {
    private int userId;
    private Integer id;
    private String title;
    private String body;

    public Post(int userId, String title, String body) {
        this.title = title;
        this.body = body;
        this.userId = userId;
    }

    public int getUserId() {
        return userId;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBordy() {
        return body;
    }

}
