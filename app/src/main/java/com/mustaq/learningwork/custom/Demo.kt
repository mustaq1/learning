package com.mustaq.learningwork.custom

fun main() {




    lefFunction()

    val firstPerson = Person("Jhon", 25, "Developer")
    val secondPerson = Person("Dev", 35, "BDE")

    val oldPerson = if (firstPerson.age >= secondPerson.age) firstPerson else secondPerson
    oldPerson.printPerson()

    run {
        if (firstPerson.age >= secondPerson.age) firstPerson else secondPerson
    }.printPerson()


    with(firstPerson) {
        age += 1
        name = "bgh"
        "Age is Now $age  and Name is Now $name"
    }.println()

    firstPerson.run {
        age += 5
        name = "Abc"
        "Age is now $age and Name is Now $name"
    }.println()


    firstPerson.let { firstPersonmodify ->
        firstPersonmodify.age += 10
        "Age is Now ${firstPersonmodify.age}"
        firstPersonmodify.name = "Mustaq"
        "Name is Now ${firstPersonmodify.name}"
        "Modification Person is $firstPersonmodify"
    }.println()


    secondPerson.apply {
        age += 23
        job = "Ghu"
    }.printPerson()

    secondPerson.also {
        it.job = "youtube"
        it.age += 45
    }.printPerson()


    println("Other")

    val home = FictionalPerson(
        firstName = "Mustaq",
        lastName = "Saiyed",
        age = 25,
        address = Address(
            number = 12547,
            state = "Gujarat",
            city = "Ahmedabad",
            street = "Vatva",
            zipCode = 382440
        ),
        gender = "Male",
        hasFacebook = true,
        hasTwitter = false,
        occupation = "Android Developer"
    )
    println(home)


    println("Apply to let function ")
    println()
    println()
    home.address.let {
        println(home.address.state)
        it.changeZipCode(12547)
        it.changeState("Ajmer")
        println(home.address.state)
    }

    val p = Person1("Adam").apply {
        age = 25
        city = "LA"
    }
    println(p)
    val student = Student(name = "Mustaq").apply {

    }
    println(student)

    val numberList = mutableListOf<Double>()
    numberList.also {
        println("populating the list")
    }.apply {
        add(9.25)
        add(3.24)
        add(1.36)
    }.also {
        println("Sorting the list")
    }.sort()
    println(numberList)


    val stingNumberList = mutableListOf("One", "Two", "Three", "Four", "Five")

    val resultList = stingNumberList.map {
        it.length
    }.filter { it > 3 }


    println("Result List $resultList")

    val countEndsWithE = stingNumberList.run {
        add("Six")
        add("Seven")
        val first = first()
        val last = last()
        println("First Item is $first Last Item is $last")
        count {
            it.endsWith("e")
        }

    }
    println("There are $countEndsWithE elements that end with e.")

    with(firstPerson){
        println("Before With  "+firstPerson.age)
        this.age=25
        println("After With  "+firstPerson.age)
    }

}

data class Student(var name: String, var mobile: Int = 0)

data class Person1(var name: String, var age: Int = 0, var city: String = "")

fun lefFunction() {
    val normalString = "NormalString"
    var length = ""
    length = if (normalString.length != null) {
        normalString?.length.toString()
    } else {
        "0"
    }
    println("Let Without $length")


    val x = normalString.let {
        it.length
    }
    println("Let Use $x")
}

data class Person(
    var name: String,
    var age: Int,
    var job: String
) {
    fun printPerson() = println(this)
}

fun String.println() = println(this)


data class FictionalPerson(
    var firstName: String,
    var lastName: String,
    var age: Int,
    var address: Address,
    var gender: String? = null,
    var hasFacebook: Boolean = false,
    var hasTwitter: Boolean = false,
    var occupation: String? = null
)

data class Address(
    var number: Int,
    var street: String,
    var city: String,
    var state: String,
    var zipCode: Int
) {
    fun changeZipCode(zipCode: Int) {
        this.zipCode = zipCode
    }

    fun changeState(state: String) {
        this.state = state
    }
}

