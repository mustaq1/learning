package com.mustaq.learningwork.custom;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.ProgressBar;

import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.UploadTask;
import com.mustaq.learningwork.R;

public class ViewDialog {

    ProgressBar progressBar;
    Dialog dialog;

    void showDialog(Context context, int percentage) {
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_progress_dialog);
        progressBar = dialog.findViewById(R.id.progressBar2);
        progressBar = new ProgressBar(context);
        progressBar.setProgress(percentage);
        dialog.show();
    }

    void hideDialog(Context context) {
        dialog.dismiss();
    }


}