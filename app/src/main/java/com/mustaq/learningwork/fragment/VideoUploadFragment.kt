package com.mustaq.learningwork.fragment

import android.Manifest
import android.content.ContentResolver
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.*
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.fragment.app.Fragment
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.mustaq.learningwork.Member
import com.mustaq.learningwork.R


class VideoUploadFragment : Fragment(), View.OnClickListener {
    companion object {
        private val TAG = "VideoUploadFragment"
    }

    lateinit var videoView: VideoView
    lateinit var btnUpload: Button
    lateinit var btnBrowse: Button
    lateinit var edNameFile: EditText
    lateinit var progressBar: ProgressBar
    lateinit var videoUri: Uri
    lateinit var mediaController: MediaController
    val PICK_VIDEO = 1
    lateinit var storageReference: StorageReference
    lateinit var databaseReference: DatabaseReference
    lateinit var member: Member
    lateinit var uploadTask: UploadTask
    lateinit var tvPercentage: TextView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_video_upload, container, false)

        member = Member()
        storageReference = FirebaseStorage.getInstance().getReference("Video")
        databaseReference = FirebaseDatabase.getInstance().getReference("video")


        videoView = view.findViewById(R.id.videoView)
        btnUpload = view.findViewById(R.id.btnUpload)
        btnBrowse = view.findViewById(R.id.btnBrowse)
        edNameFile = view.findViewById(R.id.editText)
        progressBar = view.findViewById(R.id.progressBar)
        tvPercentage = view.findViewById(R.id.tvPercentage)

        btnBrowse.setOnClickListener(this)
        btnUpload.setOnClickListener(this)

        mediaController = MediaController(requireContext())
        videoView.setMediaController(mediaController)
        videoView.start()

        return view
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnBrowse -> {
                checkRunTimePermission()
            }
            R.id.btnUpload -> {
                if (videoUri != null) {
                    uploadVideoCall()
                } else {
                    Toast.makeText(
                        requireContext(),
                        "Please select file to upload",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun checkRunTimePermission() {
        val permission = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(
                    requireContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED &&
                checkSelfPermission(
                    requireContext(),
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(permission, 11111)
            } else {
                chooseVideo()
            }
        }
    }

    private fun chooseVideo() {
        val intent = Intent()
        intent.type = "video/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(intent, PICK_VIDEO)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PICK_VIDEO) {
            if (data!!.data != null) {
                data.data?.let {
                    videoUri = data.data!!
                    videoView.setVideoURI(videoUri)
                }
            }
        }
    }

    private fun getExt(uri: Uri): String? {
        val contentResolver: ContentResolver = requireContext().contentResolver
        val mimeTypeMap = MimeTypeMap.getSingleton()
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri))
    }

    private fun uploadVideoCall() {
        val videoName = edNameFile.text.toString()
        if (videoUri != null || !TextUtils.isEmpty(videoName)) {
            progressBar.visibility = View.VISIBLE
            val reference = storageReference.child(
                System.currentTimeMillis().toString() + "." + getExt(videoUri)
            )
            uploadTask = reference.putFile(videoUri)
            uploadTask.addOnProgressListener { snapshot ->
                val progress = 100.0 * (snapshot.bytesTransferred / snapshot.totalByteCount)
                println("Upload is $progress% done")
                val currentprogress = progress.toInt()
                progressBar.progress = currentprogress
                tvPercentage.text = currentprogress.toString()
            }.addOnPausedListener {
                Toast.makeText(
                    requireContext(),
                    "Upload Pasuse",
                    Toast.LENGTH_SHORT
                ).show()
            }
            uploadTask.continueWithTask { task ->
                if (!task.isSuccessful) {
                    throw task.exception!!
                }
                reference.downloadUrl
            }
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val downloadUri = task.result
                        progressBar.visibility = View.GONE
                        Toast.makeText(requireContext(), "Data Save...", Toast.LENGTH_SHORT)
                            .show()
                        member.videoName = videoName
                        member.videoUri = downloadUri.toString()
                        val i = databaseReference.push().key
                        databaseReference.child(i!!).setValue(mediaController)
                    } else {
                        Toast.makeText(
                            requireContext(),
                            "Failed To Upload",
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
        } else {
            Toast.makeText(requireContext(), "All Field are required", Toast.LENGTH_SHORT).show()
        }
    }
}