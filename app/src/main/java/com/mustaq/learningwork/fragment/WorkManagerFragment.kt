package com.mustaq.learningwork.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.mustaq.learningwork.*
import com.mustaq.learningwork.workerManager.NotificationWorker
import retrofit2.Call
import retrofit2.Response


class WorkManagerFragment : Fragment() {

    companion object {
        val MESSAGE_STATUS = "message_status"
    }


    lateinit var btnWork: Button
    lateinit var tvWorker: TextView
    lateinit var adapterPost: AdapterPost
    lateinit var recyclerView: RecyclerView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val workView = inflater.inflate(R.layout.fragment_work_manager, container, false)

        btnWork = workView.findViewById(R.id.button)
        tvWorker = workView.findViewById(R.id.textView)
        recyclerView = workView.findViewById(R.id.rvSimplePost)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
        adapterPost = AdapterPost()

        val mWorkManager = WorkManager.getInstance()

        val mRequest =
            OneTimeWorkRequest.Builder(NotificationWorker::class.java).build()


        btnWork.setOnClickListener {
            mWorkManager.enqueue(mRequest)

        }

        mWorkManager.getWorkInfoByIdLiveData(mRequest.id).observe(requireActivity(), Observer {

            if (it != null) {
                val state: WorkInfo.State = it.state
                tvWorker.append(
                    """$state""".trimIndent()
                )
            }
        })


        getApiCall()


        return workView
    }


    private fun getApiCall() {
        RetrofitSingleTonObject
            .getRetrofitInstance
            .post
            .enqueue(object : retrofit2.Callback<List<Post>> {
                override fun onFailure(call: Call<List<Post>>, t: Throwable) {

                    Log.e(MainActivity.TAG, "${t.message}")

                }

                override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            Log.e(MainActivity.TAG, "response ${response.body()}")
                            adapterPost.setPostList(response.body()!!)
                            recyclerView.adapter = adapterPost

                        } else {
                            Toast.makeText(
                                requireActivity(),
                                "Something went wrong",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                }


            })
    }
}