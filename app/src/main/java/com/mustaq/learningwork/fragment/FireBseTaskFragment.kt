package com.mustaq.learningwork.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import com.mustaq.learningwork.R

class FireBseTaskFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val view = inflater.inflate(R.layout.fragment_fire_bse_task, container, false)


        val btnUploadVideo = view.findViewById<Button>(R.id.btnUploadVideo)
        val btnSaveData = view.findViewById<Button>(R.id.btnSaveData)
        val btnUploadImages = view.findViewById<Button>(R.id.btnUploadImages)
        val btnGoogleLogin = view.findViewById<Button>(R.id.btnGoogleLogin)
        val btnFacebook = view.findViewById<Button>(R.id.btnFacebook)
        val btnOtp = view.findViewById<Button>(R.id.btnOtp)


        btnUploadVideo.setOnClickListener {
            findNavController().navigate(R.id.action_fireBseTaskFragment_to_videoUploadActivity)
        }

        btnFacebook.setOnClickListener {
            findNavController().navigate(R.id.action_fireBseTaskFragment_to_faceBookFragment)
        }
        btnGoogleLogin.setOnClickListener {
            findNavController().navigate(R.id.action_fireBseTaskFragment_to_googleFragment)
        }
        btnUploadImages.setOnClickListener {
            findNavController().navigate(R.id.action_fireBseTaskFragment_to_uploadImagesFragment)
        }
        btnSaveData.setOnClickListener {
            findNavController().navigate(R.id.action_fireBseTaskFragment_to_firebaseAuthFragment)
        }

        btnOtp.setOnClickListener {
            findNavController().navigate(R.id.action_fireBseTaskFragment_to_mobileOtpFragment)
        }



        return view
    }



}
