package com.mustaq.learningwork.fragment

import android.content.Intent
import android.icu.text.DateTimePatternGenerator.PatternInfo.OK
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.mustaq.learningwork.R
import com.mustaq.learningwork.utils.Constants


class UploadImagesFragment : Fragment() {

    lateinit var buttonUpload: Button
    lateinit var buttonChoose: Button
    lateinit var editText: EditText
    lateinit var imageView: ImageView
    lateinit var filePath: Uri
    lateinit var storageReference: StorageReference
    lateinit var databaseReference: DatabaseReference


    companion object {
        val PICK_IMAGE_REQUEST = 234;
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val mView = inflater.inflate(R.layout.fragment_upload_images, container, false)


        buttonUpload = mView.findViewById(R.id.buttonUpload)
        buttonChoose = mView.findViewById(R.id.buttonChoose)
        editText = mView.findViewById(R.id.editText)
        imageView = mView.findViewById(R.id.imageView)

        storageReference = FirebaseStorage.getInstance().reference
        databaseReference =
            FirebaseDatabase.getInstance().getReference(Constants.DATABASE_PATH_UPLOADS)
        return mView

    }

    fun chooseFile() {
        val fileIntent = Intent()
        fileIntent.type = "images/*"
        fileIntent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(
            Intent.createChooser(fileIntent, "Select Picture"),
            PICK_IMAGE_REQUEST
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == OK) {
            filePath
        }
    }
}