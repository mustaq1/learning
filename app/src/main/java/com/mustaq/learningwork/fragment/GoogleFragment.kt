package com.mustaq.learningwork.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.mustaq.learningwork.R


class GoogleFragment : Fragment() {
    private lateinit var auth: FirebaseAuth
    private lateinit var googleSignInClient: GoogleSignInClient

    lateinit var buttonGoogle: Button
    lateinit var buttonSignOut: Button
    lateinit var tvFid: TextView
    lateinit var tvDisplayName: TextView
    lateinit var tvGivenName: TextView
    lateinit var tvFamilyName: TextView
    lateinit var tvEmail: TextView
    lateinit var imageView: ImageView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_google, container, false)


        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        googleSignInClient = GoogleSignIn.getClient(requireActivity(), gso)
        auth = Firebase.auth

        buttonGoogle = view.findViewById(R.id.buttonGoogle)
        buttonSignOut = view.findViewById(R.id.buttonSignOut)
        tvDisplayName = view.findViewById(R.id.tvDisplayName)
        tvFid = view.findViewById(R.id.tvFid)
        tvGivenName = view.findViewById(R.id.tvGivenName)
        tvFamilyName = view.findViewById(R.id.tvFamilyName)
        tvEmail = view.findViewById(R.id.tvEmail)
        imageView = view.findViewById(R.id.imageView)

        buttonGoogle.setOnClickListener {
            signIn()
        }
        buttonSignOut.setOnClickListener {
            signOut()
        }

        return view
    }

    private fun signIn() {
        val signInIntent = googleSignInClient.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    private fun signOut() {
        // Firebase sign out
        auth.signOut()
        // Google sign out
        googleSignInClient.signOut().addOnCompleteListener(requireActivity()) {
            updateUI(null)
        }
    }

    private fun revokeAccess() {
        // Firebase sign out
        auth.signOut()
        // Google revoke access
        googleSignInClient.revokeAccess().addOnCompleteListener(requireActivity()) {
            updateUI(null)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)!!
                Log.d(TAG, "firebaseAuthWithGoogle:" + account.id)
                tvDisplayName.text = account.displayName + account.familyName
                Glide.with(requireActivity())
                    .load(account.photoUrl)
                    .into(imageView)
                firebaseAuthWithGoogle(account.idToken!!)
                tvFid.text = account.id
                tvFamilyName.text = account.familyName
                tvGivenName.text = account.givenName
                tvEmail.text = account.email


            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e)
                // ...
            }
        }
    }

    // [START auth_with_google]
    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(requireActivity()) {
                Log.d(TAG, "signInWithCredential:success")
                val user = auth.currentUser
                updateUI(user)
            }
    }

    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            buttonSignOut.visibility = View.VISIBLE
            buttonGoogle.visibility = View.INVISIBLE
            tvDisplayName.text = user.displayName
            Glide.with(requireActivity())
                .load(user.photoUrl)
                .into(imageView)
            tvFid.text = user.uid
            tvFamilyName.text = user.displayName

        } else {
            tvDisplayName.text = ""
            imageView.setImageDrawable(
                ContextCompat.getDrawable(
                    requireActivity(),
                    R.drawable.ic_launcher_background
                )
            )
            tvFid.text = ""
            tvFamilyName.text = ""

            buttonSignOut.visibility = View.INVISIBLE
            buttonGoogle.visibility = View.VISIBLE
        }
    }


    companion object {
        private const val TAG = "GoogleActivity"
        private const val RC_SIGN_IN = 9001
    }
}