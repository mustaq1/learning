package com.mustaq.learningwork.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.navigation.fragment.findNavController
import com.google.android.material.textfield.TextInputLayout
import com.mustaq.learningwork.R

class FirebaseAuthFragment : Fragment() {

    lateinit var tilName: TextInputLayout
    lateinit var tilPassword: TextInputLayout

    lateinit var edEmailName: EditText
    lateinit var edPassword: EditText

    lateinit var btnSave: Button

    lateinit var tvSignUp: TextView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_firebase_auth, container, false)
        tilName = view.findViewById(R.id.tilName)
        tilPassword = view.findViewById(R.id.tilPassword)
        edEmailName = view.findViewById(R.id.edEmailName)
        edPassword = view.findViewById(R.id.edPassword)
        btnSave = view.findViewById(R.id.btnSave)
        tvSignUp = view.findViewById(R.id.tvSignUp)
        

        tvSignUp.setOnClickListener {
            findNavController().navigate(R.id.action_firebaseAuthFragment_to_saveDataFragment)
        }
        btnSave.setOnClickListener {
            uploadDataFirebase()
        }
        return view
    }

    private fun uploadDataFirebase() {
        when {
            edEmailName.text.isEmpty() -> {
                tilName.error = "Please enter name"
            }
            edPassword.text.isEmpty() -> {
                tilPassword.error = "Please enter password"
            }
            else -> {
                uploadData()
            }
        }
    }

    private fun uploadData() {

    }

}