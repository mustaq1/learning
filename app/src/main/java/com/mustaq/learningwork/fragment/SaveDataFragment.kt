package com.mustaq.learningwork.fragment

import android.os.Bundle
import android.util.Patterns.EMAIL_ADDRESS

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton


import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.mustaq.learningwork.R
import com.mustaq.learningwork.model.UserModel
import java.util.regex.Pattern


class SaveDataFragment : Fragment() {

    private lateinit var database: DatabaseReference
    lateinit var userModel: UserModel

    lateinit var tilName: TextInputLayout
    lateinit var edName: EditText
    lateinit var tilPassword: TextInputLayout
    lateinit var edPassword: EditText
    lateinit var maleRadioButton: RadioButton
    lateinit var femaleRadioButton: RadioButton
    lateinit var btnSave: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_save_data, container, false)
        database = FirebaseDatabase.getInstance().getReference("users")
        userModel = UserModel()



        btnSave = view.findViewById(R.id.btnSave)
        tilName = view.findViewById(R.id.tilName)
        tilPassword = view.findViewById(R.id.tilPassword)


        edName = view.findViewById(R.id.edName)
        edPassword = view.findViewById(R.id.edPassword)



        btnSave.setOnClickListener {
            if(checkValidation()){

            }
        }



        return view

    }

    private fun checkValidation(): Boolean {
        var success = false
        when {
            edName.text.isEmpty() -> {
                tilName.error = "Please Enter Name"
                success = false
            }
            edPassword.text.isEmpty() -> {
                tilPassword.error = "Please Enter Password"
            }

        }
        return success
    }

    fun EditText.emailValid():Boolean{


        return true
    }

    private fun createUser(
        _firstName: String,
        _lastName: String,
        _age: Int,
        _mobile: Int,
        _gender: String,
        _city: String,
        _state: String,
        _country: String,
        _imageUrl: String
    ) {
        val user = UserModel(
            _firstName,
            _lastName,
            _age,
            _mobile,
            _gender,
            _city,
            _state,
            _country,
            _imageUrl
        )
        database.setValue(user)
    }

    companion object {
        val TAG = "SaveDataFragment"
    }
}