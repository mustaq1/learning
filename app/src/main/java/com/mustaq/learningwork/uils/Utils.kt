package com.mustaq.learningwork.uils

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide

fun View.visible(isVisible: Boolean) {
    visibility = if (isVisible) View.VISIBLE else View.GONE
}

fun ImageView.loadImage(url: String) {
    Glide.with(this)
        .load(url)
        .into(this)
}

fun View.isEnabled(vararg views: View) {
    for (v in views) {
        v.isEnabled = true
    }
}

fun View.isDisabled(vararg views: View) {
    for (v in views) {
        v.isEnabled = true
    }
}

 fun enableViews(vararg views: View) {
    for (v in views) {
        v.isEnabled = true
    }
}

 fun disableView(vararg views: View) {
    for (v in views) {
        v.isEnabled = false
    }
}
