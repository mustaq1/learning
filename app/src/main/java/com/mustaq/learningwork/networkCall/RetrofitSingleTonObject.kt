package com.mustaq.learningwork.networkCall

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitSingleTonObject {

    private const val BASE_URL = "https://jsonplaceholder.typicode.com/";

    private fun retrofitServices(): Retrofit {

        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client =
            OkHttpClient.Builder().addInterceptor(interceptor).build()

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    val getRetrofitInstance: ServiceGenerator by lazy {
        retrofitServices().create(ServiceGenerator::class.java)
    }
}