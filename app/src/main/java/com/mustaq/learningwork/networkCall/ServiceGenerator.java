package com.mustaq.learningwork.networkCall;

import com.mustaq.learningwork.model.Comment;
import com.mustaq.learningwork.model.Post;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface ServiceGenerator {


    @GET("posts")
    Call<List<Post>> getPost();

    @POST("posts")
    Call<Post> postData(@Body Post post);


    @GET("posts")
    Call<List<Post>> getPostUsingQuery(@Query("userId") int userID);


    //if we want to pass multiple user id so we can easy to pass array
    // ex:- @Query("userId") Integer[] userID
    @GET("posts")
    Call<List<Post>> getPostUsingMultipleQuery(
            @Query("userId") Integer[] userID,
            @Query("_sort") String Sort,
            @Query("_order") String order);

    //Query map
    @GET("posts")
    Call<List<Post>> getPostUsingQueryHasMap(@QueryMap Map<String, String> stringStringMap);


    @GET("posts/{id}/comments")
    Call<List<Comment>> getComment(@Path("id") int postId);

    @GET
    Call<List<Comment>> getCommentUsingUrl(@Url String url);
}
